package service;

import pojo.BookType;
import pojo.Books;

import java.util.List;

public interface BookService {
    void AddBook(String codeId,String title);
    List<Books> QueryBook(String title);
    List<BookType> findBookType();
}
