package service;

import dao.BookMapper;
import org.springframework.stereotype.Service;
import pojo.BookType;
import pojo.Books;

import javax.annotation.Resource;
import java.util.List;
@Service
public class BookServiceImpl implements BookService {
    @Resource
    private BookMapper bookMapper;



    public void setBookMapper(BookMapper bookMapper) {
        this.bookMapper = bookMapper;
    }

    @Override
    public void AddBook(String codeId, String title) {
        bookMapper.AddBook(codeId,title);
    }

    @Override
    public List<Books> QueryBook(String title) {
        return bookMapper.QueryBook(title);
    }

    @Override
    public List<BookType> findBookType(){ return  bookMapper.findBookType(); }

}
