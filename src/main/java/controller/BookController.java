package controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import pojo.BookType;
import pojo.Books;
import service.BookService;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class BookController {
    @Resource
    private BookService bookService;

    @RequestMapping("/book/queryBook")
    public String QueryBookController(Model model, String title) {
        System.out.println(title);
        List<Books> list = bookService.QueryBook(title);
        System.out.println(list);
        model.addAttribute("list", list);
        return "/book/queryBookResults";
    }

    /*@RequestMapping("/book/addBook")
    public void AddBookController(Model model, String codeid, String title) {
        bookService.AddBook(codeid, title);
        model.addAttribute("successmessage", "添加图书成功！");
    }*/

    @RequestMapping("/book/addBookFindType")
    public String findBookType(Model model) {
        List<BookType> alltypelist = bookService.findBookType();
        model.addAttribute("alltypelist", alltypelist);
        System.out.println(alltypelist);
        return "/book/addBook";
    }

    @RequestMapping("/book/addBook")
    public String AddBookController(Model model, String title,String codeId) {
        bookService.AddBook(codeId,title);
        model.addAttribute("successMessage", 1);
/*        try {
            sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/
        return "redirect:/book/addBookFindType";
    }

}
