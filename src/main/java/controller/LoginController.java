package controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller

public class LoginController {

    @RequestMapping("/main/login")
    public String execute(String username, String userpass) {
        if (username.equals("admin") && userpass.equals("123456")) {
            return "redirect:/main/main.jsp";
        }
        else{
            return "redirect:/main/login.jsp";
        }
    }
}