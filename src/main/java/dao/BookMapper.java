package dao;

import org.apache.ibatis.annotations.Param;
import pojo.BookType;
import pojo.Books;

import java.util.List;

public interface BookMapper{
    void AddBook(@Param("codeId") String codeId,@Param("title") String title);
    List<Books> QueryBook(String title);
    List<BookType> findBookType();
}
